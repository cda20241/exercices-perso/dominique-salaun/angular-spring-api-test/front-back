import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './composants/login/login.component';
import { PageNotFoundComponent } from './composants/page-not-found/page-not-found.component';
import { AcceuilComponent } from './composants/acceuil/acceuil.component';
import { UserComponent } from './composants/user/user.component';
import { AdminComponent } from './composants/admin/admin.component';

const routes: Routes = [
{path:"", redirectTo:'login',pathMatch:"full"},
{path:"login", component:LoginComponent},
{path:"acceuil", component:AcceuilComponent},
{path:"user", component:UserComponent},
{path:"admin", component:AdminComponent},
{path:"**", component: PageNotFoundComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
