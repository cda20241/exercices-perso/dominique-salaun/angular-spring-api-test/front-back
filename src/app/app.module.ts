import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './composants/login/login.component';
import { UserComponent } from './composants/user/user.component';
import { AdminComponent } from './composants/admin/admin.component';
import { AuthComponent } from './composants/auth/auth.component';
import { NavbarComponent } from './composants/navbar/navbar.component';
import { AcceuilComponent } from './composants/acceuil/acceuil.component';
import { PageNotFoundComponent } from './composants/page-not-found/page-not-found.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AcceuilComponent,
    AdminComponent,
    AuthComponent,
    LoginComponent,
    NavbarComponent,
    UserComponent,
    AppComponent,
    PageNotFoundComponent
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
