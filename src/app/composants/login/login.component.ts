import { Component } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { HttpClient } from '@angular/common/http';
import { Account } from 'src/app/interfaces/account';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(
    private http: HttpClient,
    private accountService: AccountService,
  ) {}

  ngOnInit(): void {
  }

  login(email: string, password: string) {
    console.log(email);
    let account = {} as Account;
    account.email = email;
    account.password = password;
    console.log(account);
    this.accountService.login(account).subscribe(
      (account: Account) => {
        this.accountService.accountLoggedSuccessfully(account);
      },
    );
  }
}
