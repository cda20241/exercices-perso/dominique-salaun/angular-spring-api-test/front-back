import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <div class='center' routerLink="/login">
      <img src="https://th.bing.com/th/id/R.2a6ee1e785843ed666fdc2405f70044e?rik=7gqUgWJ1ht%2fqfw&riu=http%3a%2f%2fwww.onedesignonline.fr%2fimg%2f404.png&ehk=lh8qUkujSpiTExMtOzxZUjIC3dsm9yLv%2b3d4iEvhNyA%3d&risl=&pid=ImgRaw&r=0"/>
      
      <a routerLink="/acceuil" class="waves-effect waves-teal btn-flat">
        Retourner à l'accueil
      </a>
    </div>
  `,
  styles: []
})
export class PageNotFoundComponent {}
