import { Component, OnInit } from '@angular/core';
import { AccountService } from './services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front-back';

  constructor(private accountService:AccountService){}

  isLogged(): boolean {
    return this.accountService.isLogged();
  }
}
