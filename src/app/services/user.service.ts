import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Account } from '../interfaces/account';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl = environment.apiUrl + "/api/v1/demo-controller"

  constructor(private http : HttpClient) { }

  getUser(){
    return this.http.get<Account[]>(this.apiUrl)
  
  }
}
