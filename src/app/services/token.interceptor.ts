import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccountService } from './account.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private accountService: AccountService,) { }



intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
const token = this.accountService.account.token;
if (token !== null){
  let clone = request.clone({
    headers: request.headers.set('authorization','Bearer' + token)
  })
  return next.handle(clone)
}
else{
  return next.handle(request)
}
  }
}

export const TokenInterceptProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptor,
  multi: true
}

