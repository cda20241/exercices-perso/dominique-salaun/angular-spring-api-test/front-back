import { Injectable } from '@angular/core';
import { Account, AccountImpl } from '../interfaces/account';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  apiUrl = environment.apiUrl + "/api/v1/auth";
  account: Account = new AccountImpl();

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  login(account: Account): Observable<Account> {
    console.log(this.apiUrl);
    this.account = account;
    return this.http.post<Account>(this.apiUrl + '/authenticate', account);
  }

  accountLoggedSuccessfully(account: Account) {
    this.account = account;
    this.router.navigate(['/acceuil']);
  }

  isLogged(): boolean {
    console.log(this.account.token);
    const token = this.account.token;
    return !!token;
  }

  logout() {
    // Effacez les informations d'identification de l'utilisateur et redirigez-le vers la page de connexion
    this.account = new AccountImpl();
    this.router.navigate(['/login']);
  }


}
